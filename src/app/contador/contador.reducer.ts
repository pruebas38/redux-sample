import { Action } from "@ngrx/store";
// import { actions, DECREMENTAR, DIVIDIR, INCREMENTAR, MULTIPLICAR, RESET } from "./contador.actions";
// En lugar de importar una por una
import * as contadorAction from "./contador.actions";


export function contadorReducer (state: number = 10, action: contadorAction.actions) {

  switch(action.type) {
    case contadorAction.INCREMENTAR:
      return state + 1;
    case contadorAction.DECREMENTAR:
      return state - 1;
    case contadorAction.MULTIPLICAR:
      return state * action.payload;
    case contadorAction.DIVIDIR:
      return state / action.payload;
    case contadorAction.RESET:
      return 0;
    default:
      return state;
  }

}
