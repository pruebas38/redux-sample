import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { DividirAction, MultiplicarAction } from '../contador.actions';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styles: [
  ]
})
export class HijoComponent implements OnInit {

  contador: number;

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.select('contador')
    .subscribe(contador => {
      this.contador = contador;
    });
  }

  multiplicar() {
    const accion = new MultiplicarAction(5);
    this.store.dispatch(accion);
  }

  dividir() {
    const accion = new DividirAction(5);
    this.store.dispatch(accion);
  }

  resetNieto(event) {
    this.contador = event;
    // this.cambioContador.emit(this.contador);
  }

}
